# svganimedit

SVG Editor that handles also animations!
You may test the editor at the following address: [http://people.irisa.fr/Francois.Schwarzentruber/svganimedit/](http://people.irisa.fr/Francois.Schwarzentruber/svganimedit/).


## Roadmap (things to be done)
- layout of the website (quite urgent, the project needs an expert in css and layout)
- editor of path, and some svg elements