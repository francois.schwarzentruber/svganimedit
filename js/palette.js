/**
 * Creation of the palette (the tools on the left)
 */
function paletteInit() {
    paletteNewSection("Objects");
    addButton("line", '<line x1="100" y1="0" x2="100" y2="100" \n         style="stroke:black;stroke-width:2" />');
    
    addButton("circle", '<circle cx="50" cy="200" r="20" \n fill="#aeaeae" />');
    
    addButton("text", '<text x="100" y="100" text-anchor="middle"  \n fill="black" font-size="20px" font-family="Arial" dy=".3em">et</text>');
    
    addButton("polygon", '<polygon points="200,10 250,190 160,210"/>');
    
    paletteNewSection("Animation");
    addButton("animate attribute", '<animate attributeName="cy" attributeType="XML"\n             from="300"  to="220"\n             begin="0s" dur="2s"\n              fill="remove"/>');
    
    addButton("animate rotate",
    '<animateTransform attributeName="transform"\n' +
                         ' attributeType="XML"\n' +
                         ' type="rotate"\n' +
                         ' from="0 60 70"\n' +
                          'to="360 60 70"\n' +
                          'begin="0s"\n' +
                          'dur="10s"\n' +
                          'repeatCount="indefinite"/>');
    
    
    $( "#palette" ).accordion();
    
}




let paletteCurrentSection = null;


/**
 * Create a new section (a new set of buttons)
 * @param {String} name 
 */
function paletteNewSection(name) {
    $( "#palette" ).append("<h3>" + name + "</h3>");
    paletteCurrentSection = $("<div>");
    $( "#palette" ).append(paletteCurrentSection);

}

/**
 * Add a button, when clicking on it, it adds xmlCode in the code
 * @param {*} name 
 * @param {*} xmlCode 
 */
function addButton(name, xmlCode) {
    let button = $("<button>" + name + "</button>");
    button.on("click", () => editor.replaceSelection(xmlCode + "\n"));
    paletteCurrentSection.append(button);
}


