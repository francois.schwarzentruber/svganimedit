function isSVGElementAnimate(element) {
    return (element.localName == "animate") || (element.localName == "animateTransform");
}




function getSVGNode(nodeType, values) {
    let node = document.createElementNS("http://www.w3.org/2000/svg", nodeType);
    for (var p in values)
         node.setAttributeNS(null, p, values[p]);
    return node;
  }
