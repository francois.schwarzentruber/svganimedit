/**
 * main function that initializes everything
 */
$(document).ready(function() {
  editorInit();
  paletteInit();
  sliderInit();
  update();

  loop();
});



