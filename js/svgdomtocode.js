/**
 * add onclick events that pinpoints relevant part of the code
 */
function addGoToCodeToSVGElements() { addGoToCodeToSVGElementsSub(getSVGTree(), getSVG()); }





/**
 * add onclick events that pinpoints relevant part of the code
 * @param positionalTree is the syntactic tree of the code in the editor provided by positionalxmlparse
 *    (it contains in particular information about the position in the text of the different elements)
 * @param tree is the real syntactic tree in the DOM, that is effectively displayed.
 */
function addGoToCodeToSVGElementsSub(positionalTree, tree) {
    if(tree == undefined) return;
    if(tree.localName != "svg")
    tree.onclick = function() {
        console.log(positionalTree)
        selectElementInEditor(tree);
    }

    positionalTree.domElement = tree;
    tree.data = positionalTree;

    let j = 0;
    for(let i = 0; i < positionalTree.children.length; i++) {
        if(tree.children[j] == undefined) {
            console.log("error in addGoToCodeToSVGElementsSub");
            console.log(positionalTree);
            console.log(tree);
            return;
        }
        
        if(tree.children[j].getAttribute("class") == "highlight") j++;
        addGoToCodeToSVGElementsSub(positionalTree.children[i], tree.children[j]);
        j++;
    }
}





