let isPaused = false;

function getSVG() {
    return $('#output svg')[0];
}


/**
 * update the displayed SVG (update also the slider, and copy the internal state of the SVG)
 */
function update() { 
    console.log("update");

    function updateSVGButSameCurrentTime() {
        let t = getSVG().getCurrentTime();
        $("#output").html(getDecoratedSVGXMLCode());
        addGoToCodeToSVGElements();
        if(t > getDuration()) t = getDuration();
        getSVG().setCurrentTime(t);

        timelineUpdate();

        
        $(".highlight").css( "animation-iteration-count" , "infinite" );
        $(".highlight").css( "animation" , "shake 0.5s" );
    }
    
    function updateSlider() {
        $( "#slider" ).slider( "option", "max", getDuration() ) ;
    }

    updateSVGButSameCurrentTime();
    updateSlider();

    if(isPaused) {
        pause();
        $( "#slider" ).change();
    }
}




/**
 * @returns the duration of the SVG
 */
function getDuration() {
    let max = 0;
  $("animate, animateTransform, animateMotion").each(function() {
      let self = this;
      let t = parseInt($(self).attr("begin")) + parseInt($(self).attr("dur"));
      if(max < t ) max = t;
  });
  return max;
}

/**
 * pause the animation
 */
function pause() { $("#buttonPause").hide(); $("#buttonStart").show(); getSVG().pauseAnimations(); isPaused = true;};


/**
 * restart the animation (from the time it was paused)
 */
function start() {
     $("#buttonPause").show();
     $("#buttonStart").hide();
     getSVG().unpauseAnimations(); isPaused = false; loop();
}


/**
 * loop function that updates the slider
 */
function loop() {
    if(!isPaused) {
        let t = getSVG().getCurrentTime();       
        $( "#slider" ).slider( "option", "value", t);
        timelineUpdateCurrentTime();
        showTime();
        setTimeout(loop, 50);
    }
}



/**
 * pause and set the current time of the SVG animation from the value of the slider
 */
function pauseAndUpdateCurrentTimeFromSlider() {
    pause();
    var value = $( "#slider" ).slider( "option", "value" );
    getSVG().setCurrentTime(value);
    getSVG().pauseAnimations();
    
}



function sliderInit() {
    $( "#slider" ).slider({
        max: 0,
        step: 0.001,
        width: 400,
        mousedown: function() {
            pause();
        },
        slide: function(event, ui) {
            showTime();
            pauseAndUpdateCurrentTimeFromSlider();
            timelineUpdateCurrentTime();
        }
    } );
}




function showTime() {
    t = Math.round($( "#slider" ).slider( "option", "value") * 1000);
    t = t / 1000;
    $("#t").html(t + "s");
    //$( "#custom-handle" ).text(t + "s");
}