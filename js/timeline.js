function timeToX(t) {
    if(getDuration() == 0) return 0;
    return t * $("#slider").width() / getDuration();
}



function timelineUpdate() {
    let rows = [];

    function rowToY(row) {
        return 8 + 20 * row;
    }

    console.log("timelineUpdate")
    console.log(getSelectedElement( ))

    $("#timeline").empty();


    function timelineSVGElementVisit(element) {

        function getTimeLineRow(t) {
            for(let row = 0; row < rows.length; row++) {
                if(rows[row] <= t)
                    return row;
            }
            return rows.length;
        }

        function addAnimateElementInTimeLine(element) {
            let begin = parseFloat(element.getAttribute("begin"));
            let dur = parseFloat(element.getAttribute("dur"));

            let row = getTimeLineRow(begin);
            let y = rowToY(row);

            rows[row] = begin + dur;

            let lineClass;
            let selectedElement = getSelectedElement();

            if((selectedElement.localName != "svg") && (selectedElement.data.begin.begin <= element.data.begin.begin) && (element.data.end.end <= selectedElement.data.end.end))
                lineClass = "timelineselected";
            else
                lineClass = "timelineunselected";
           

            let line = getSVGNode("rect", {x: timeToX(begin), y: y, width: timeToX(dur), height: 10, class: lineClass});

            line.onclick = () => selectElementInEditor(element);
            
            $("#timeline")[0].appendChild(line);
        }

        if(isSVGElementAnimate(element)) {
            addAnimateElementInTimeLine(element);
            
        } else {
            for(let child of element.children)
            if(child.getAttribute("class") != "highlight")
                timelineSVGElementVisit(child);
        }
    }
    
    
    timelineSVGElementVisit(getSVG());

    let y = rowToY(rows.length);
    $("#timeline").attr("height", y+4);

    for(let t = 0; t < getDuration(); t++) {
        $("#timeline")[0].appendChild(getSVGNode("line", {x1: timeToX(t), y1: 0, x2: timeToX(t), y2: y, class: "timelineverticalline"}));
    }

    let t = getSVG().getCurrentTime();
    $("#timeline")[0].appendChild(getSVGNode("line", {x1: timeToX(t), y1: 0, x2: timeToX(t), y2: y, id: "timelineverticallinecurrent"}));

    
}



function timelineUpdateCurrentTime() {
    let t = getSVG().getCurrentTime();
    $("#timelineverticallinecurrent").attr("x1",  timeToX(t));
    $("#timelineverticallinecurrent").attr("x2",  timeToX(t));
}