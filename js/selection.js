/**
 * @returns the selected element
 */
function getSelectedElement() {
    function augmentWithPos(c) {
        let p = 0;
        for(let n = 0; n < c.line; n++)
            p += 1 + editor.getLine(n).length;
        c.pos = p + c.ch;
        return c;
    }

    function getSubTree(tree, c) {
        for(let child of tree.children)
            if(child.data != undefined)
            if((child.data.begin.begin <= c.pos) && (c.pos <= child.data.end.end))
                return getSubTree(child, c);
        return tree;
    }

    let cur = editor.getCursor();
    if(editor.getSelection() != "") //there is a bug in editor: when there is a selection, the cursor is just afterwards
        cur.pos--;
    return getSubTree(getSVG(), cur);
}