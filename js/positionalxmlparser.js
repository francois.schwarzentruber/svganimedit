function getTreeFromTokens(tokens) {
    if(tokens[0].type == "beginend") {
        return {tree: {begin: tokens[0], end: tokens[0], children: []}, tokens: tokens.slice(1)};
    }
    else if(tokens[0].type == "begin") {
        let tree = {begin: tokens[0], children: []};
        tokens = tokens.slice(1);
        while(tokens.length > 0 && tokens[0].type != "end") {
            let result = getTreeFromTokens(tokens);
            tree.children.push(result.tree);
            tokens = result.tokens;
        }
        tree.end = tokens[0];
        return {tree: tree, tokens: tokens.slice(1)};
    } else {
        console.log("ERROR");
        return undefined;
    }
}


/**
 * @example positionalxmlparse("<div><circle/></div>")
 * @param {*} code 
 */
function positionalxmlparse(code) {
    return getTreeFromTokens(lexer(code)).tree;
}

/**
 * @example lexer("<div></div>")
 * @returns the list of tokens of the XML code string code
 * @param {*} code 
 */
function lexer(code) {
    let tokens = [];
    let token = {begin: 1, end: 0};
    let modeString = false;
    let modeComment = false;
    
    function pushToken() { 
        if(token.type != undefined) {
            token.text = code.substring(token.begin, token.end+1);
            tokens.push(token);
        }
    }

    while(token.end < code.length) {
        let i = token.end;

        if(modeComment == true) {
            if(code[i] ==">") {
                token.end = i;
                pushToken();
                token = {begin: i+1, end: i+1};
                modeComment = false;
            }
            else
                token.end++;
        }
        else if(modeString == true) {
            if(code[i]== '"') {
                token.end++;
                modeString = false;
            }
            else
                token.end++;
        }
        else {
            if(code[i] == '"') {
                modeString = true;
                token.end++;
            }
            else if(code[i] == "<" & code[i+1] == "/") {
                token.end = i-1;
                pushToken();
                token = {type: "end", begin: i, end: i+2};
            }
            else if(code[i] == "<" & code[i+1] == "!") {
                token.end = i-1;
                pushToken();
                token = {type: "beginend", comment: true, begin: i, end: i+2};
            }
            else if(code[i] == "<" & code[i+1] == "?") {
                token.end = i-1;
                pushToken();
                token = {type: "beginend", comment: true, begin: i, end: i+2};
            }
            else if(code[i] == "<") {
                pushToken();
                token = {type: "begin", begin: i, end: i+1};
            }
            else if(code[i] =="/" && code[i+1] == ">") {
                token.type = "beginend";
                token.end = i+1;
                pushToken();
                token = {begin: i+2, end: i+2};
                
            }
            else if(code[i] ==">") {
                token.end = i;
                pushToken();
                token = {begin: i+1, end: i+1};
            }  
            else
                token.end++;
        }       
    }

    return tokens;
}




function tokensPrettyPrint(tokens) {
    for(let token of tokens) {
        if(token.type != undefined)
            console.log(token.type + ": " + token.text);
    }
}