let editor;
  

function editorInit() {
    editor = CodeMirror.fromTextArea(document.getElementById("input"), {
        lineWrapping: true,
        lineNumbers: true,
        extraKeys: {"Ctrl-Space": "autocomplete"},
        lint: true
      });
      editorResize();
      
      editor.on("change", update);
      editor.on("cursorActivity", update);
      $( "#codePanel" ).resizable();
    
}

function editorResize() {


   // editor.setSize("100%", (window.innerHeight - 0) + "px");
   editor.setSize("100%", window.innerHeight - 50);

   let left = $('#codePanel').width();
   let top = $('#codePanel').offset().top;
    let newwidth = window.innerWidth - left - 180;
    $('#svgPanel').css({"max-width": newwidth, "width": newwidth, "max-height": (window.innerHeight - top) + "px",
    "height": (window.innerHeight - top) + "px"});
}


window.onresize = editorResize;

String.prototype.splice = function(idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};









/**
 * returns the xmlCode that corresponds to the SVG to display with highlights etc. that comes from the editor
 */
function getDecoratedSVGXMLCode() {


/**
 * 
 * @param {*} code 
 * @returns the SVG code code, plus the element that is highlighted
 */
function addSelectedElement(code) {

    /**
     * @returns an object that contains the information about the element in the editor "<circle>...</circle>"
     */
    function getElement() {
        function augmentWithPos(c) {
            let p = 0;
            for(let n = 0; n < c.line; n++)
                p += 1 + editor.getLine(n).length;
            c.pos = p + c.ch;
            return c;
        }

        function getSubTree(tree, c) {
            for(let child of tree.children)
                if((child.begin.begin <= c.pos) && (c.pos <= child.end.end))
                    return getSubTree(child, c);
            return tree;
        }

        return getSubTree(getSVGTree(), augmentWithPos(editor.getCursor()));
    }

    let el = getElement();
    if(el == undefined) return code;

    el.xmlCode = editor.getValue().substring(el.begin.begin, el.end.end+1);
    let eldom = $(el.xmlCode);
    if(eldom.length > 1) return code; //trick to see that getElement() is not a real element like "</circle>..."
    if(eldom.is("svg"))
        return code;

    if(eldom.is("animate"))
        return code;

    eldom.attr("style", "fill:none;stroke:yellow;stroke-width:15");
    eldom.addClass("highlight");
    return code.splice(el.begin.begin, 0, eldom[0].outerHTML);
}
     return addSelectedElement(editor.getValue());
} 




/**
 * selects the portion of text that corresponds to the SVG node element (some data are stored in the field data)
 * @param {*} positionalTree 
 */
function selectElementInEditor(element) {
     editor.setSelection(getCursor(element.data.begin.begin), getCursor(element.data.end.end+1));
}






/**
 * selects the portion of text that corresponds to the SVG node element (some data are stored in the field data)
 * @param {*} positionalTree 
 */
function selectElementInEditorFromData(elementData) {
    console.log(elementData);
    editor.setSelection(getCursor(elementData.begin.begin), getCursor(elementData.end.end+1));
}



/**
 * @returns an object {line: l, ch: c} where l is the line number and c is the number of characters from the left
 * on the line
 * @param {*} position the index in the code of the SVG
 */
function getCursor(position) {
    let p = 0;
    for(let l = 0; l < editor.lineCount(); l++) {
        let p2 = p + editor.getLine(l).length;
        if(p <= position && (position <= p2))
            return {line: l, ch: position-p};
        p = p2+1;

    }
    console.log("ERROR getCursor");
    return undefined;
}



/**
 * @returns the syntactic tree of the editor
 */
function getSVGTree() {
    return positionalxmlparse(editor.getValue());
}




/**
 * reinitialize (clear) the editor
 */
function editorClear() {
    editor.setValue('<svg xmlns="http://www.w3.org/2000/svg" width="200" height="350" version="1.1">\n\n</svg>');
    editor.setCursor({line: 1, ch: 2});
}

